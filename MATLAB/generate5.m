function fiveToneWav = generate5(digits,protocol)
    %Permet de générer la matrice WAV pour les 5 tons (ou plus)
    %   on entre les digits sous forme d'une chaîne de caractères
    %   on choisit parmi les protocoles suivants:
    %   1)ZVEI 1
    %   2)ZVEI 2
    %   3)ZVEI 3
    %   4)CCIR
    %   5)EEA
    %   6)EIA
    %   7)EUROSIGNAL
    %Génération des sinus nécessaires pour chaque protocole
    Fe = 48000;
    fiveToneWav = []; %on déclare la sortie comme une matrice
    tones = [;]; %pour la suite de nombres et ABCDEF
    %on va modifier le message envoyé pour detecter les répétitions
    for i=2:length(digits)
        if digits(i-1) == digits(i)
            %on remplace le digit actuel par un E
            digits(i) = 'E';
        end
    end
    switch(protocol)
        case 'ZVEI1'
            tones(1, :) = synthad(0.8, 1060, 0.07, Fe); %1
            tones(2, :) = synthad(0.8, 1160, 0.07, Fe); %2
            tones(3, :) = synthad(0.8, 1270, 0.07, Fe); %3
            tones(4, :) = synthad(0.8, 1400, 0.07, Fe); %4
            tones(5, :) = synthad(0.8, 1530, 0.07, Fe); %5
            tones(6, :) = synthad(0.8, 1670, 0.07, Fe); %6
            tones(7, :) = synthad(0.8, 1830, 0.07, Fe); %7
            tones(8, :) = synthad(0.8, 2000, 0.07, Fe); %8
            tones(9, :) = synthad(0.8, 2200, 0.07, Fe); %9
            tones(10, :) = synthad(0.8, 2400, 0.07, Fe); %0
            tones(11, :) = synthad(0.8, 2800, 0.07, Fe); %A
            tones(12, :) = synthad(0.8, 810, 0.07, Fe); %B
            tones(13, :) = synthad(0.8, 970, 0.07, Fe); %C
            tones(14, :) = synthad(0.8, 885, 0.07, Fe); %D
            tones(15, :) = synthad(0.8, 2600, 0.07, Fe); %E
            tones(16, :) = synthad(0.8, 680, 0.07, Fe); %F
        case 'ZVEI2'
            tones(1, :) = synthad(0.8, 1060, 0.07, Fe); %1
            tones(2, :) = synthad(0.8, 1160, 0.07, Fe); %2
            tones(3, :) = synthad(0.8, 1270, 0.07, Fe); %3
            tones(4, :) = synthad(0.8, 1400, 0.07, Fe); %4
            tones(5, :) = synthad(0.8, 1530, 0.07, Fe); %5
            tones(6, :) = synthad(0.8, 1670, 0.07, Fe); %6
            tones(7, :) = synthad(0.8, 1830, 0.07, Fe); %7
            tones(8, :) = synthad(0.8, 2000, 0.07, Fe); %8
            tones(9, :) = synthad(0.8, 2200, 0.07, Fe); %9
            tones(10, :) = synthad(0.8, 2400, 0.07, Fe); %0
            tones(11, :) = synthad(0.8, 885, 0.07, Fe); %A
            tones(12, :) = synthad(0.8, 810, 0.07, Fe); %B
            tones(13, :) = synthad(0.8, 740, 0.07, Fe); %C
            tones(14, :) = synthad(0.8, 680, 0.07, Fe); %D
            tones(15, :) = synthad(0.8, 970, 0.07, Fe); %E
            tones(16, :) = synthad(0.8, 2600, 0.07, Fe); %F
        case 'ZVEI3'
            tones(1, :) = synthad(0.8, 970, 0.07, Fe); %1
            tones(2, :) = synthad(0.8, 1060, 0.07, Fe); %2
            tones(3, :) = synthad(0.8, 1160, 0.07, Fe); %3
            tones(4, :) = synthad(0.8, 1270, 0.07, Fe); %4
            tones(5, :) = synthad(0.8, 1400, 0.07, Fe); %5
            tones(6, :) = synthad(0.8, 1530, 0.07, Fe); %6
            tones(7, :) = synthad(0.8, 1670, 0.07, Fe); %7
            tones(8, :) = synthad(0.8, 1830, 0.07, Fe); %8
            tones(9, :) = synthad(0.8, 2000, 0.07, Fe); %9
            tones(10, :) = synthad(0.8, 2200, 0.07, Fe); %0
            tones(11, :) = synthad(0.8, 885, 0.07, Fe); %A
            tones(12, :) = synthad(0.8, 2800, 0.07, Fe); %B
            tones(13, :) = synthad(0.8, 810, 0.07, Fe); %C
            tones(14, :) = synthad(0.8, 2600, 0.07, Fe); %D
            tones(15, :) = synthad(0.8, 2400, 0.07, Fe); %E
            tones(16, :) = synthad(0.8, 680, 0.07, Fe); %F
        case 'CCIR'
            tones(1, :) = synthad(0.8, 1124, 0.1, Fe); %1
            tones(2, :) = synthad(0.8, 1197, 0.1, Fe); %2
            tones(3, :) = synthad(0.8, 1275, 0.1, Fe); %3
            tones(4, :) = synthad(0.8, 1358, 0.1, Fe); %4
            tones(5, :) = synthad(0.8, 1446, 0.1, Fe); %5
            tones(6, :) = synthad(0.8, 1540, 0.1, Fe); %6
            tones(7, :) = synthad(0.8, 1640, 0.1, Fe); %7
            tones(8, :) = synthad(0.8, 1747, 0.1, Fe); %8
            tones(9, :) = synthad(0.8, 1860, 0.1, Fe); %9
            tones(10, :) = synthad(0.8, 1981, 0.1, Fe); %0
            tones(11, :) = synthad(0.8, 2400, 0.1, Fe); %A
            tones(12, :) = synthad(0.8, 930, 0.1, Fe); %B
            tones(13, :) = synthad(0.8, 2247, 0.1, Fe); %C
            tones(14, :) = synthad(0.8, 991, 0.1, Fe); %D
            tones(15, :) = synthad(0.8, 2110, 0.1, Fe); %E
            %tones(16, :) = synthad(0.8, 2600, 0.1, Fe); %F
        case 'EEA'
            tones(1, :) = synthad(0.8, 1124, 0.04, Fe); %1
            tones(2, :) = synthad(0.8, 1197, 0.04, Fe); %2
            tones(3, :) = synthad(0.8, 1275, 0.04, Fe); %3
            tones(4, :) = synthad(0.8, 1358, 0.04, Fe); %4
            tones(5, :) = synthad(0.8, 1446, 0.04, Fe); %5
            tones(6, :) = synthad(0.8, 1540, 0.04, Fe); %6
            tones(7, :) = synthad(0.8, 1640, 0.04, Fe); %7
            tones(8, :) = synthad(0.8, 1747, 0.04, Fe); %8
            tones(9, :) = synthad(0.8, 1860, 0.04, Fe); %9
            tones(10, :) = synthad(0.8, 1981, 0.04, Fe); %0
            tones(11, :) = synthad(0.8, 1055, 0.04, Fe); %A
            tones(12, :) = synthad(0.8, 930, 0.04, Fe); %B
            tones(13, :) = synthad(0.8, 2400, 0.04, Fe); %C
            tones(14, :) = synthad(0.8, 991, 0.04, Fe); %D
            tones(15, :) = synthad(0.8, 2110, 0.04, Fe); %E
            %tones(16, :) = synthad(0.8, 2600, 0.07, Fe); %F
        case 'EIA'
            tones(1, :) = synthad(0.8, 741, 0.033, Fe); %1
            tones(2, :) = synthad(0.8, 882, 0.033, Fe); %2
            tones(3, :) = synthad(0.8, 1023, 0.033, Fe); %3
            tones(4, :) = synthad(0.8, 1164, 0.033, Fe); %4
            tones(5, :) = synthad(0.8, 1305, 0.033, Fe); %5
            tones(6, :) = synthad(0.8, 1446, 0.033, Fe); %6
            tones(7, :) = synthad(0.8, 1587, 0.033, Fe); %7
            tones(8, :) = synthad(0.8, 1728, 0.033, Fe); %8
            tones(9, :) = synthad(0.8, 1869, 0.033, Fe); %9
            tones(10, :) = synthad(0.8, 600, 0.033, Fe); %0
            tones(11, :) = synthad(0.8, 2151, 0.033, Fe); %A
            tones(12, :) = synthad(0.8, 2433, 0.033, Fe); %B
            tones(13, :) = synthad(0.8, 2010, 0.033, Fe); %C
            tones(14, :) = synthad(0.8, 2292, 0.033, Fe); %D
            tones(15, :) = synthad(0.8, 459, 0.033, Fe); %E
            %tones(16, :) = synthad(0.8, 2600, 0.033, Fe); %F
        case 'EURO'
            tones(1, :) = synthad(0.8, 903.1, 0.1, Fe); %1
            tones(2, :) = synthad(0.8, 832.5, 0.1, Fe); %2
            tones(3, :) = synthad(0.8, 767.4, 0.1, Fe); %3
            tones(4, :) = synthad(0.8, 707.4, 0.1, Fe); %4
            tones(5, :) = synthad(0.8, 652, 0.1, Fe); %5
            tones(6, :) = synthad(0.8, 601, 0.1, Fe); %6
            tones(7, :) = synthad(0.8, 554, 0.1, Fe); %7
            tones(8, :) = synthad(0.8, 510.7, 0.1, Fe); %8
            tones(9, :) = synthad(0.8, 470.8, 0.1, Fe); %9
            tones(10, :) = synthad(0.8, 979.8, 0.1, Fe); %0
            tones(11, :) = synthad(0.8, 433.9, 0.1, Fe); %A
            tones(12, :) = synthad(0.8, 400, 0.1, Fe); %B
            tones(13, :) = synthad(0.8, 368.7, 0.1, Fe); %C
            tones(14, :) = synthad(0.8, 1153.1, 0.1, Fe); %D
            tones(15, :) = synthad(0.8, 1062.9, 0.1, Fe); %E
            tones(16, :) = synthad(0.8, 339.9, 0.1, Fe); %F
        otherwise
             fprintf('OOF! Protocole non défini!');
             return
    end
    %on parcourt les 5 tons en entrée et on les place dans la matrice
    for i=1:length(digits)
        if hex2dec(digits(i)) > 0 && hex2dec(digits(i)) < 10
            fiveToneWav = [fiveToneWav tones(hex2dec(digits(i)), :)];
        elseif digits(i) == '0'
            fiveToneWav = [fiveToneWav tones(10, :)];
        elseif hex2dec(digits(i)) + 1 > 10 && hex2dec(digits(i)) + 1 <= 16
            fiveToneWav = [fiveToneWav tones(hex2dec(digits(i)) + 1, :)];
        else
            fprintf('Mhh...Il y a un caractère qui ne va pas...');
            return
        end
    end
end

